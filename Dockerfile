FROM learningit/restaurant-backend:latest

# File Transfer
COPY ./restaurant-backend /var/www/html
COPY ./container-startup.sh /root
COPY ./000-default.conf /etc/apache2/sites-available/000-default.conf

# Modify permissions
RUN chmod 777 /root/container-startup.sh
RUN chmod -R 777 /var/www/html

# Run Composer Script
RUN /root/container-startup.sh

# Give Permission after composer update
RUN chmod -R 777 /var/www/html

# Active Routing in Apache
RUN a2enmod rewrite
RUN service apache2 restart
