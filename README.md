##  Relevant Files
Any files in the root directory of this repository are needed for infrastructure and pipeline/cicd purposes.

Within the folder "base_docker_file" is a Dockerfile containing any constructions, that are not dynamic. Its image is published to the docker repo at docker.io (learningit/restaurant-backend:latest) and is not build within the pipeline to decrease the runtime of the pipline to speed up the actual build time.

The folder "restaurant-backend" contains the actual application:

**composer.json**
Contains all dependencies for this application:

-  PHP version 8

-  laravel/lumen-framework

-  mongodb/mongodb

-  mustangostang/spyc

Composer must be installed on the host system. Afterwards, the command "composer update" installs all the dependencies listed within the composer.json file. 
Note: composer is automatically installed by the instructions in the Dockerfile.

**/public/index.php**
Within apache the /public directory is configured as root dir, so that /public/index.php will be accessed automatically whenever an HTTP-request addresses the domain "dasistallesnurge.cloud". The code in index.php simply starts the application and lets the magic happen. Whenever the dependencies are not installed correctly (check description of composer.json) it throws an error, since it cannot load /../bootstrap/app.php.

**/routes/web.php**
Contains any route and its functionality. With Lumen Framework it is possible to specify custom routes and define its HTTP-method, as well as the parameters that are passed as and json-body. The actual code for each route is outsourced to several php-files within the /routes directory. The reason for this is a segmentation of use cases to keep a good code-overview.

**/assets/functions.php**
Contains several functions that are called by the single route-functions in /routes/web.php. The code was not put directly into the route-functions to keep a better overview. Also this allows reusing single functions in multiple route-functions to reduce code-duplicity. The functions have telling names that suit the different business use cases. The functions call another set of functions, that wrap simple CRUD-operations for MongoDB management -> /dbfunctions/DB.php

**/dbfunctions/DB.php**
Contains a static class DB. The class is static, because this way the connection to the MongoDB is established one single time, which reduces the number of active sessions (to avoid hitting any limit set by MongoDB-service) as well as network load. It contains all functions for creating, editing, deleting and receiving MongoDB documents. 

## Pipeline Installation Routine
CI/CD Pipeline which contains two stages:
- publish
- deploy

Both stages are executed by a preinstalled Gitlab-Runner on the Linux-Server. 

**publish**
Contains all steps to build the Docker Image based on a predefinded Dockerfile. The base Dockerfile is located in the root of the repository. 

script: 
- docker build -t $TAG_COMMIT -t $TAG_LATEST:                               Building the Docker Image based on the predefined Dockerfile. 
- docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY:          Logging into the Projects Container Registry on Gitlab. 
- docker push $TAG_COMMI & $TAG_LATEST:                                     Pushing the built Docker Image to the Container Registry on Gitlab. 

**deploy**
Contains all steps to pull the Docker Image from the Gitlab Container Registry and to build and deploy a Docker Container based on it the Linux Server. All script steps are executed one the Linux-Server, where the Docker Container is going to be deployed.

script:
- docker system prune -a -f:                                                Removing all unused containers, images and networks on the Linux Server. 
- docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY:          Logging into the Projects Container Registry on Gitlab. 
- docker pull $TAG_COMMIT:                                                  Pulling the Docker Image from the Container Registry on Gitlab. 
- docker container rm -f backend-restaurant || true:                        Removing a container named backend-restaurant. 
- docker run -t -d -p 80:80 --name backend-restaurant $TAG_COMMIT:          Running a Docker Container named backend-restaurant which is based on the pulled Docker Image. 

##  Service Structure (Infrastructure-Architecture)
![Service Structure](https://gitlab.com/jkw12345/backend-restaurant/-/raw/main/restaurant-backend/GrafikenDiagramme/SystemAbbildIcons.png "Service Structure")
  

##  Code Architecture
![Code Architecture](https://gitlab.com/jkw12345/backend-restaurant/-/raw/main/restaurant-backend/GrafikenDiagramme/SoftwarearchitekturAbbild.png "Code Architecture")
  
  

##  Routes Documentation
The documentation of every route can be found here:
https://gitlab.com/jkw12345/backend-restaurant/-/blob/main/restaurant-backend/Documentation/Backend_Restaurant.postman_collection.json-SwaggerYaml1.yaml
  

##  Helpful Links
-  Lumen Framework: https://lumen.laravel.com/docs/9.x
-  DigitalOcean: https://www.digitalocean.com
-  GitLab CI/CD: https://docs.gitlab.com/ee/ci/
-  Docker: https://www.docker.com
