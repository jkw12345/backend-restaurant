<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;

//shows the menu of a specific restaurant
$router->get('/restaurant/{restaurant_id}/speisekarte', function (Request $request, $restaurant_id) use ($router) {
    
    try{
        $restaurant = DB::readDocument($restaurant_id);
        $body = getSpeisekarteFromRestaurant($restaurant);    
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
});

//show a specific dish of the menu
$router->get('/gericht/{gericht_id}', function (Request $request, $gericht_id) use ($router) {
    try{
        //returns the dish that belongs to the "gericht_id" given in the URL 
        $body = DB::bson2Json(DB::readDocument($gericht_id));
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
});

//delete the menu which belongs to the "speisekarte_id" given in the URL
$router->delete('/speisekarte/{speisekarte_id}', function (Request $request, $speisekarte_id) use ($router) {
    try{
        $body =DB::deleteDocument($speisekarte_id);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
    
});



//upload a menu of a restaurant
$router->post('/speisekarte', function (Request $request) use ($router) {
    try{
        //receives the menu from the HTTP-request, decodes the data and saves it in $speisekarte
        $speisekarte = json_decode($request->getContent());
        //saves the restaurantid which was given in a parameter
        $restaurantid = $request->input("restaurantid");
        //creates the new menu with the information of $speisekarte and the reference to the restaurant of $restaurantid
        $id = createSpeisekarteObject($speisekarte, $restaurantid);
        //returns the id of the new menu
        $body = createReturnId($id);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
});


//add a dish to a existing menu
$router->patch('/speisekarte/{speisekarte_id}/add', function (Request $request, $speisekarte_id) use ($router) {
    try{
        //creates a new dish with the information of the HTTP-request
        $oid = createGericht($request);
        // add the new dish to the menu that belongs to $speisekarte_id given in the URL
        $body = DB::addOidToDocumentArray($speisekarte_id, $oid);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
    
});


// delete a dish of a menu
$router->patch('/speisekarte/{speisekarte_id}/remove/{gericht_id}', function (Request $request, $speisekarte_id, $gericht_id) use ($router) {
    try{
    // Remove Mapping from Speisekarten-Array
    DB::removeOidFromDocumentArray($speisekarte_id, $gericht_id);
    // Delete Gericht
    DB::deleteDocument($gericht_id);
    $body = NULL;
    return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
    
});
