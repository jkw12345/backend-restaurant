<?php
use Illuminate\Http\Request;
use Illuminate\Http\Response;


//show a "tischplan" of a restaurant
$router->get('/restaurant/{restaurant_id}/tischplan', function (Request $request, $restaurant_id) use ($router) {
    try{
    //reads the file that belongs to $restaurant_id given in the URL and saves it in $restaurant
    $restaurant = DB::readDocument($restaurant_id);
    //returns the "tischplan" of the restaurant that belongs to $restaurant_id
    $body = getTischplanFromRestaurant($restaurant);
    return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }

});

//show a specific table
$router->get('/tisch/{tisch_id}', function (Request $request, $tisch_id) use ($router) {
    try{
        //reads the file that belongs to $tisch_id given in the URL and saves it in $tisch
        $tisch = DB::readDocument($tisch_id);
        //returns $tisch in JSON-Format
        $tisch = json_decode(DB::bson2Json($tisch));
        
        $body = json_encode($tisch);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
    
});