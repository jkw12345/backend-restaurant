<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;


// order "Gericht"
$router->post('/bestellung', function (Request $request) use ($router) {
    

    try{
        // creates Bestellung-object and writes it to the DB 
        $bestellungObject = createBestellungObject($request);
        // creates object with ID of Bestellung and its price and returning it as Response afterwards
        $body = returnBestellungObject($bestellungObject);
        return (new Response($body,200))
            ->header('Content-Type','application/json');
    }catch(Exception $e){
        $body = json_encode(
            array(
                "errormessage" => $e->getMessage()
            )    
        );
            return ( new Response($body,500))
                ->header('Content-Type','application/json');
    } 
});

// change status of specific Bestellung-object
$router->patch('/bestellung', function (Request $request) use ($router) {
    

    try{
        // receiving params from HTTP-request and unset id-entry from array, so that iteration through other params within DB::updateDocument 
        // method is possible -> $id is saved since it is needed as identifier and is not handled as attribute, that should be updated like the other values in $params
        $params = $request->input();
        $id = $params["id"];
        unset($params["id"]);
        // patching the Bestellung in the DB by handing in the params, that should be updated and identifiying the Bestellung-object with $id
        $body = DB::updateDocument($id, $params);
        return (new Response($body,200))
            ->header('Content-Type','application/json');
    }catch(Exception $e){
        $body = json_encode(
            array(
                "errormessage" => $e->getMessage()
            )    
        );
            return ( new Response($body,500))
                ->header('Content-Type','application/json');
    }
});

// get status of specific Bestellung-object
$router->get('/bestellung/{bestellung_id}/status', function (Request $request, $bestellung_id) use ($router) {
    try{
        // Receiving Bestellung object with DB::readDocument and pass it to returnBestellStatus. Returning the output of returnBestellStatus afterwards.
        $body = returnBestellStatus(DB::readDocument($bestellung_id));
        return (new Response($body,200))
            ->header('Content-Type','application/json');
    }catch(Exception $e){
        $body = json_encode(
            array(
                "errormessage" => $e->getMessage()
            )    
        );
            return ( new Response($body,500))
                ->header('Content-Type','application/json');
    }
});

// cancel/delete specific Bestellung-object
$router->delete('/bestellung/{bestellung_id}', function (Request $request, $bestellung_id) use ($router) {
    try{
        // deleting Bestellung-object in DB with simple CRUD-operation within DB::deleteDocument by its id
        $body = DB::deleteDocument($bestellung_id);
        return (new Response($body,200))
            ->header('Content-Type','application/json');
    } catch(Exception $e){
        $body = json_encode(
            array(
                "errormessage" => $e->getMessage()
            )    
        );

        return ( new Response($body,500))
            ->header('Content-Type','application/json');
    }    
});
