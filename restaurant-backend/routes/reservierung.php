<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;

// show all Reserverierung-objects of a specific user
$router->get('/user/{user_id}/reservierungen', function (Request $request, $user_id) use ($router) {
    try {
        // Receiving all Reservierung-objects with getReservierungen, then filtering it to a 
        // specific user by his id with and formatting the output to a returnable object (json) with formatReservierungObject
        $body = formatReservierungObject(getReservierungen(), $user_id);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
});

// cancel/delete specific Reservierung-object
$router->delete('/reservierung/{reservierung_id}', function (Request $request, $reservierung_id) use ($router) {
    try {
        // deleting Reservierung-object in DB with simple CRUD-operation within DB::deleteDocument by its id
        $body = DB::deleteDocument($reservierung_id);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
            array(
                "errormessage" => $e->getMessage()
            )
        );
        //var_dump($e->getMessage());
        return (new Response($body, 500))
            ->header('Content-Type', 'application/json');
    }
});

// creating Reservierung-object and writing it to the DB
$router->post('/reservierung', function (Request $request) use ($router) {
    try {
        // creating Reservierung-object by the HTTP-request data -> there is also a mapping created to the table that should be reserved in the DB
        $reservierungObject = createReservierungObject($request);

        // returning json-data containing the ID of the new Reservierung-object
        $body = createReturnId($reservierungObject["Id"]);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
            array(
                "errormessage" => $e->getMessage()
            )
        );
        //var_dump($e->getMessage());
        return (new Response($body, 500))
            ->header('Content-Type', 'application/json');
    }
});
