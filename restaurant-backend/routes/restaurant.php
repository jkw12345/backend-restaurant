<?php
use Illuminate\Http\Request;
use Illuminate\Http\Response;
// show all restaurants - filtering by specific attributes is also possible
$router->get('/restaurants', function (Request $request) use ($router) {
    try{
        // receiving HTTP-request data 
        $restaurantFilter = $request->input();

        // getting all restaurants from the database -> if $restaurantFilter is not empty, the specified filters are applied when querying the DB
        $result = DB::readDocumentsOfType("Restaurant", $restaurantFilter);


        // converting the data from bson to raw json and returning it
        $body = DB::bson2Json($result);
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    } catch (Exception $e) {
        $body = json_encode(
        array(
            "errormessage" => $e->getMessage()
        )
    );
    //var_dump($e->getMessage());
    return (new Response($body, 500))
        ->header('Content-Type', 'application/json');
    }
});