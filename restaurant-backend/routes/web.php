<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


// Loading Dependencies
require("../assets/functions.php");
require("../authentication/functions.php");
require("../dbfunctions/DB.php");


//Authentication
include("../routes/authentication.php");

//Restaurants
include("../routes/restaurant.php");

//Reservierung
include("../routes/reservierung.php");

//Speisekarte
include("../routes/speisekarte.php");

//Bestellung
include("../routes/bestellung.php");

//Tisch und Tischplan
include("../routes/tisch.php");