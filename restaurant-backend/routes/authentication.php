<?php
use Illuminate\Http\Request;
use Illuminate\Http\Response;


// User Login
$router->get('/user/login', function (Request $request) use ($router) {

    // Get values from request
    $usermail = trim($request->input("usermail"));
    $passhash = trim($request->input("passhash"));

    // check for user and pass
    $loginValidation = validateLogin($usermail, $passhash);

    // if no user in db
    if ($loginValidation == NULL){
        $body = json_encode(
            array(
                "userid" => "NULL"
            )
        );
        return (new Response($body, 401))
            ->header('Content-Type', 'application/json');
    }

    // if user in db and correct password
    elseif(!$loginValidation == NULL){
        $body = json_encode(
            array(
                "userid" => $loginValidation
            )
        );
        return (new Response($body, 200))
            ->header('Content-Type', 'application/json');
    }

    // just in case
    else {
        $body = json_encode(
            array(
                "userid" => "NULL"
            )
        );

        return (new Response($body, 500))
            ->header('Content-Type', 'application/json');
    }
});



// User Register
$router->post('/user/register', function (Request $request) use ($router) {
    //receive usermail and passhash through parameters
    $usermail = trim($request->input("usermail"));
    $passhash = trim($request->input("passhash"));

    $userid = registerUser($usermail, $passhash);
    $body = json_encode(
        array(
            "userid" => $userid
        )
    );
    if(empty($usermail) || empty($passhash)){
        
        return (new Response($body, 401))
        ->header('Content-Type', 'application/json');
        return registerUser($usermail, $passhash);

    }
    else {

        return (new Response($body, 200))
        ->header('Content-Type', 'application/json');
        return registerUser($usermail, $passhash);
    }

});