<?php
function validateLogin($usermail, $passhash){
    $users = (getAllUsers())['users'];

    $userid = NULL;
    foreach ($users as $user){
        if ($user["usermail"] == $usermail && $user["passhash"] == $passhash){
            $userid = $user["userid"];
        }
    } 
    return $userid;
}

function registerUser($usermail, $passhash){

    // No new user should be registered, if same username already exists!
    if (checkUserExistence($usermail) == true){
        return "user already exists!";
    }

    // Usermail and Passhash are not empty

    if(empty($usermail) || empty($passhash)){
        return "Email und oder Passwort dürfen nicht leer sein";
    }

    $userid = rand(0001,9999);

    // Create new user array
    $newUser = array( 
        "userid" => $userid,
        "usermail" => $usermail,
        "passhash" => $passhash
    );

    // Load current users from YML
    $userYml = getAllUsers();

    // Add new User
    array_push($userYml['users'], $newUser);

    // Convert to YML and write to file
    $Spyc = new Spyc();
    $dump = $Spyc->dump($userYml);
    $fp = fopen('../temp/users.yml', 'w');
    fwrite($fp, $dump);
    fclose($fp);

    return $userid;

}

function getAllUsers(){
    $userYml = Spyc::YAMLLoad("../temp/users.yml");
    return $userYml;
    
}

function checkUserExistence($usermail){
    $users = (getAllUsers())['users'];
    $userExists = false;
    foreach ($users as $user){
        if ($user["usermail"] == $usermail){
            $userExists = true;
        }
    }
    return $userExists;
}
