<?php

function getUserMailByIndex($index){
    $Data = Spyc::YAMLLoad("../assets/test.yml");
    $Mail = $Data['users'][$index]['mail'];
    return $Mail;
}



function cus_dump($obj = array()) {
    echo "<pre>";
    print_r($obj);
    echo "</pre>";
}


function arrayKeyFinder($array,$wildcard){
    $keys = array_keys($array);
    $keylist = array();
    foreach ($keys as $key){
        if (str_contains($key,$wildcard)){
            array_push($keylist,$key);
        }
    }
    return $keylist;
}



function getSpeisekarteFromRestaurant($restaurant){
    $restaurant = json_decode(DB::bson2Json($restaurant));
    $oid = '$oid';
    $speisekarte = $restaurant->Speisekarte->$oid;
    $speisekarte = DB::readDocument($speisekarte);
    $a = json_decode(DB::bson2Json($speisekarte));
    $a = (array)$a;

    $output = array();
    $keys = arrayKeyFinder($a,"Gericht");
    foreach ($keys as $gericht){
        $output[$gericht] = $a[$gericht]->$oid;
    }
    return json_encode($output);
}


function getPricefromOrder($order){
    $price = 0;
    foreach($order as $element){
        $price = $price + ($element->preis * $element->anzahl);
    }
    return $price;
}

function replaceArrayIdWithBsonID($array){
    $newarray = array();
    foreach ($array as $entry){
        $attribute = key($entry);
        $entry->$attribute= new MongoDB\BSON\ObjectID($entry->$attribute);
        array_push($newarray,$entry);
    }
    return $newarray;
}

function getTischplanFromRestaurant($restaurant){
    $restaurant = json_decode(DB::bson2Json($restaurant));
    $oid = '$oid';
    $tischplan = $restaurant->Tischplan->$oid;
    $tischplan = DB::readDocument($tischplan);
    $a = json_decode(DB::bson2Json($tischplan));
    $a = (array)$a;

    $output = array();
    $keys = arrayKeyFinder($a,"Tisch");
    foreach ($keys as $tisch){
        $output[$tisch] = $a[$tisch]->$oid;
    }
    return json_encode($output);
}

function createBestellungObject($request){
    $order = json_decode($request->getContent()); 
    $userid = $request->input("userid");
    $kategorie = $request->input("kategorie");

    $object = new stdClass();
    $object->UserID = new MongoDB\BSON\ObjectID($userid);
    $object->Kategorie = $kategorie;
    $object->Bestellung = replaceArrayIdWithBsonID($order);
    $object->Preis = getPricefromOrder($order);
    $object->Status = "Angenommen";
    $object->Bewertung_Sterne = "NA";
    $object->Bewertung_Text = "";
    $object->_typ = "Bestellung";

    $id = DB::createDocument($object);

    return array(
        "Id" => $id,
        "Object" => $object
    );
}

function returnBestellungObject($array){
    $returnobject = new stdClass();
    $returnobject->ID = (string)$array["Id"];
    $returnobject->Preis = $array["Object"]->Preis;

    return json_encode($returnobject); 
}


function returnBestellStatus($bestellung){
    $bestellung = json_decode(DB::bson2Json($bestellung));
    $returnobject = new stdClass();
    $returnobject->status = $bestellung->Status;  
    return json_encode($returnobject); 
}

function getReservierungen(){
    $result = DB::readDocumentsOfType("Reservierung");
    $result = json_decode(DB::bson2Json($result));
    return $result;
}

function formatReservierungObject($result, $user_id){
    $kundenreservierungen = [];
    $oid = '$oid';
    foreach($result as $res){
        if ($user_id == $res->Kunde->$oid){
            array_push($kundenreservierungen, $res);
        }
    }
    return json_encode($kundenreservierungen);
}

function createReservierungObject($request){
    $userId = $request->input("userid");
    $start = $request->input("start");
    $ende = $request->input("ende");
    $tischId = $request->input("tischid");
   
    //TODO: Klasse hieraus machen
    $object = new stdClass();
    $object->Kunde = new MongoDB\BSON\ObjectID($userId);
    $object->Start = $start;
    $object->Ende = $ende;
    $object->_typ = "Reservierung";
    $id = (string )DB::createDocument($object);

    return array(
        "Id" => $id,
        "TischId" => $tischId
    );
}


function createReturnId($id){
    $returnobject = new stdClass();
    $returnobject->ID = (string)$id;
    return json_encode($returnobject);
}


function createSpeisekarteObject($parameter, $restaurantid){
    $object = new stdClass();
    $object->RestaurantID = new MongoDB\BSON\ObjectID($restaurantid);
    $object->Gerichte = replaceArrayIdWithBsonID($parameter);
    $object->_typ = "Speisekarte";
    $id = DB::createDocument($object);
    return $id;
}


function createGericht($request){
    // Gericht anlegen
    $gericht = (array) json_decode($request->getContent()); 
    $gerichtpreis = (double)$gericht["Preis"];
    $gerichttyp = "Gericht";
    unset($gericht["Preis"]);

    $object = new stdClass();
    $keys = array_keys($gericht);
    foreach($keys as $key){
        $object->$key = $gericht[$key];
    }
    $object->Preis = new MongoDB\BSON\Decimal128($gerichtpreis);
    $object->_typ = "Gericht";

    // Gericht der Karte hinzufügen
    $oid = (string) DB::createDocument($object);
    return $oid;
}