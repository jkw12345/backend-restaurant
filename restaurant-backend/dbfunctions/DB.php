<?php
# https://www.mongodb.com/developer/quickstart/php-crud/
class DB {
    public static function connectMongoDB(){
        // TODO: Hardcoding in Config File auslagern
        $client = new MongoDB\Client('mongodb+srv://backend:Passwort@cluster0.f3grm.mongodb.net');
        return $client;
    }

    public static function getMongoCollection(){
        // TODO: Hardcoding von Colleciton-Name in Config File auslagern
        return DB::connectMongoDB()->Test_Database->datenbank;
    }

    public static function createDocument($object){
        $collection = DB::getMongoCollection();
        $insertOneResult = $collection->insertOne($object);
        return $insertOneResult->getInsertedId();
    }

    public static function readDocument($id){
        $collection = DB::getMongoCollection();

        return $collection->findOne(
            ['_id' => new MongoDB\BSON\ObjectID($id)]
        );
    }

    public static function readDocumentsOfType($type, $filter = NULL){
        $collection = DB::getMongoCollection();
        $params = array(
            '_typ' => $type 
        );

        if(!is_null($filter)){
            $params = array_merge($params,$filter);
        }


        $cursor = $collection->find(
            $params,
            [
                'limit' => 100
            ]
        );

        return $cursor;


    }

    public static function updateDocument($id, $array){
        $collection = DB::getMongoCollection();
        $updateResult = $collection->updateOne(
            ['_id' => new MongoDB\BSON\ObjectID($id)],
            //[ '$set' => [ 'username' => 'test1', 'email' => "test2" ]]
            [ '$set' => $array]
        );

    }


    //TODO: "Gerichte" generisch machen (auch in der remove-Funktion)
    public static function addOidToDocumentArray($documentid, $oid){
        $collection = DB::getMongoCollection();
        $updateResult = $collection->updateOne(
            ['_id' => new MongoDB\BSON\ObjectID($documentid)],
            ['$push' => array("Gerichte" => new MongoDB\BSON\ObjectID($oid))]
        );

    }

    public static function addOidToDocument($documentid, $oid){
        $collection = DB::getMongoCollection();
        $updateResult = $collection->updateOne(
            ['_id' => new MongoDB\BSON\ObjectID($documentid)],
            ['$set' => ["Reservierung_1" => new MongoDB\BSON\ObjectID($oid)]]
        );

    }

    public static function removeOidFromDocumentArray($documentid, $oid){
        $collection = DB::getMongoCollection();
        $updateResult = $collection->updateOne(
            ['_id' => new MongoDB\BSON\ObjectID($documentid)],
            ['$pull' => array("Gerichte" => new MongoDB\BSON\ObjectID($oid))]
        );

    }

    public static function deleteDocument($id){
        $collection = DB::getMongoCollection();
        $deleteResult = $collection->deleteOne(
            ['_id' => new MongoDB\BSON\ObjectID($id)]
        ); 
    }

    public static function bson2json($bson){
        if ($bson instanceof MongoDB\Driver\Cursor){
            $bson = ($bson->toArray());
        }
        $json = MongoDB\BSON\toJSON(MongoDB\BSON\fromPHP($bson));
        return $json;
    }
}